#include "log.h"

#include "inttypes.h"
#include "stdarg.h"
#include "stdio.h"

__attribute__((weak))
void halt() {
#if CPU_ARM
  __asm__("bkpt #0");
#endif
  while (1) {}
}

void warn(char *string) {
  puts("warn: ");
  puts(string);
  puts("\n");
}

void logf_(const char *file, uint32_t line, const char *func, char *format, ...) {
  // TODO: Add time to log message.
  printf("%s:%" PRIu32 ": %s(): ",
         file, line, func);
  va_list args;
  va_start(args, format);
  vprintf(format, args);
  va_end(args);
  puts("\n");
}
