#pragma once

#ifndef UNUSED
#define UNUSED(x) (void)(x)
#endif // UNUSED

#define UNUSED_FUNC(var) \
    void *__macro_UNUSED_FUNC_##var = ((void *)(var))

#ifndef NULL
#define NULL 0
#endif // NULL

#if TEST
#define TEST_OR_STATIC
#else // !TEST
#define TEST_OR_STATIC static
#endif // TEST

#define MAX(a,b) \
  ((a) < (b) ? (b) : (a))

#define MIN(a,b) \
  ((a) < (b) ? (a) : (b))

#define CLAMP(min,x,max) \
  MAX(min, MIN(x, max))

#define CMP(a,b)                                \
  (                                             \
   ((a) <  (b)) ? -1 :                          \
   ((a) == (b)) ?  0 : 1                        \
  )
